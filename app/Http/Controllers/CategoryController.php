<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use DataTables;
use App\User;
use Image;

class CategoryController extends Controller
{
    //
    public function showCategory(Request $request)
    {
        if ($request->ajax()) {
            $categories = Category::latest()->get();
            return DataTables::of($categories)
                ->addColumn('action', function ($categories) {
                    $button = '<button type="button" name="edit" id="' . $categories->id . '" class="edit btn btn-primary btn-sm">Edit</button>';
                    $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="' . $categories->id . '" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Category::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                    $url = $row->getFirstMediaUrl('categoryImages');
                    return '<img src=' . $url . ' alt="" height="50" width="80" style="object-fit:contain" />';
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="/admin/categories/' . $row['id'] . '/edit" class="edit btn btn-primary btn-sm"> <i class="fa fa-edit"></i></a>';
                    $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/categories/' . $row['id'] . '/delete" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.categories.index');
    }
    public function restore($id)
    {
        $category = Category::withTrashed()->find($id);
        if (empty($category)) {
            return view('admin.trash', ['#pills-category']);
        }
        $category->restore();
        return redirect()->route('admin-trash', ['#pills-category'])->with('success', 'Category restore successfully!');
    }
    public function permanentDelete($id)
    {
        $category = Category::withTrashed()->find($id);
        if (empty($category)) {
            return redirect()->route('admin-trash', ['#pills-category']);
        }
        $category->forceDelete();
        return redirect()->route('admin-trash', ['#pills-category'])->with('success', 'Category deleted permanently!');
    }

    public function destroy($id)
    {
        $user = Category::find($id)->delete();
        return redirect()->route('admin-categories')->with('success', 'Category deleted successfully!');
    }
    public function create()
    {
        $categories = Category::all();

        return view('admin.categories.create', ["categories" => $categories]);
    }
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit', ['category' => $category]);
    }

    public function store(Request $request)
    {

        var_dump(request()->all());
        request()->validate([
            'categoryName' => ['required', 'max:255', 'min:2', 'unique:categories,name'],
            'categoryDescription' => ['required', 'min:5'],
            'categoryImage' => ['required', 'max:2048', 'image'],
        ]);
        $category = new Category();
        if (request()->parentCategory != "") {
            $category->parent_id = request()->parentCategory;
        }
        $category->name = request('categoryName');
        $category->description = request('categoryDescription');


        //upload file
        // if ($request->hasfile('categoryImage')) {
        //     $img_tmp = $request->file('categoryImage');

        //     $extension = $img_tmp->getClientOriginalExtension();
        //     $filename = rand(111, 99999) . '.' . $extension;
        //     $img_path = 'uploads/categories/' . $filename;
        //     //image resize
        //     Image::make($img_tmp)->resize(500, 500)->save($img_path);
        //     $category->image = $filename;
        // }


        $category->slug = str_slug(request('categoryName'), "-");
        $category
            ->addMediaFromRequest('categoryImage')
            ->toMediaCollection('categoryImages');
        $category->save();
        return redirect('/admin/categories')->with('success', 'Category created successfully!');
        // code...
    }

    public function update($id)
    {
        $category = Category::find($id);
        request()->validate([
            'categoryName' => ['required', 'max:255', 'min:2'],
            'categoryDescription' => ['required', 'min:5'],
            'categoryImage' => ['max:2048', 'image'],
        ]);
        $category->name = request('categoryName');
        $category->description = request('categoryDescription');
        if (request()->hasfile('categoryImage')) {

            $category->clearMediaCollection('categoryImages');

            $category->addMediaFromRequest('categoryImage')->toMediaCollection('categoryImages');
        }
        $category->save();
        return redirect('/admin/categories')->with('success', 'Category updated successfully!');
    }
}
