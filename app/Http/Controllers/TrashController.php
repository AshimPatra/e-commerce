<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\User;
use DataTables;


class TrashController extends Controller
{
    public function index()
    {
        return view('admin.trash');
    }
    public function userTrash(Request $request)
    {
        if ($request->ajax()) {
            $data = User::onlyTrashed()->get();
            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('action', function ($row) {
                    $btn = '<a href="/admin/users/' . $row['id'] . '/restore" class="edit btn btn-primary btn-sm">restore</a>';
                    $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/users/' . $row['id'] . '/permanentDelete" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.users.trash');
    }
    public function ProductTrash(Request $request)
    {

        if ($request->ajax()) {
            $data = Product::onlyTrashed()->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('creator', function ($row) {
                    return $row->creator->name;
                })
                ->addColumn('image', function ($row) {
                    $url = $row->getFirstMediaUrl('productImages');
                    return '<img src=' . $url . ' alt="" height="50" width="80" style="object-fit:contain" />';
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="/admin/products/' . $row['id'] . '/restore" class="edit btn btn-primary btn-sm">restore</a>';
                    $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/products/' . $row['id'] . '/permanentDelete" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.products.trash');
    }
    public function CategoryTrash(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::onlyTrashed()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                    $url = $row->getFirstMediaUrl('categoryImages');
                    return '<img src=' . $url . ' alt="" height="50" width="80" style="object-fit:contain" />';
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="/admin/categories/' . $row['id'] . '/restore" class="edit btn btn-primary btn-sm">restore</a>';
                    $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/categories/' . $row['id'] . '/permanentDelete" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.categories.trash');
    }
}
