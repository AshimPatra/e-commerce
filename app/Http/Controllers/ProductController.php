<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use DataTables;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\product_category;
use Illuminate\Support\Facades\Input;
use Image;

class ProductController extends Controller
{
  public function showProducts(Request $request)
  {
    if ($request->ajax()) {
      $products = Product::latest()->get();
      return DataTables::of($products)
        ->addColumn('action', function ($products) {
          $button = '<button type="button" name="edit" id="' . $products->id . '" class="edit btn btn-primary btn-sm">Edit</button>';
          $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="' . $products->id . '" class="delete btn btn-danger btn-sm">Delete</button>';
          return $button;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
  }
  public function index(Request $request)
  {
    if ($request->ajax()) {
      $data = Product::latest()->get();
      return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('creator', function ($row) {
          return $row->creator->name;
        })
        ->addColumn('image', function ($row) {
          $url = $row->getFirstMediaUrl('productImages');
          return '<img src=' . $url . ' alt="" height="50" width="80" style="object-fit:contain" />';
        })
        ->addColumn('action', function ($row) {
          $btn = '<a href="/admin/products/' . $row['id'] . '/edit" class="edit btn btn-primary btn-sm"> <i class="fa fa-edit"></i></a>';
          $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/products/' . $row['id'] . '/delete" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
          return $btn;
        })
        ->rawColumns(['action', 'image'])
        ->make(true);
    }


    return view('admin.products.index');
  }

  public function restore($id)
  {

    $product = Product::withTrashed()->find($id);
    if (empty($product)) {
      return view('admin.trash', ['#pills-product']);
    }
    $product->restore();
    return redirect()->route('admin-trash', ['#pills-product'])->with('success', 'Product restore successfully!');
  }
  public function permanentDelete($id)
  {
    $product = Product::withTrashed()->find($id);
    if (empty($product)) {
      return redirect()->route('admin-trash', ['#pills-product']);
    }
    $product->forceDelete();
    return redirect()->route('admin-trash', ['#pills-product'])->with('success', 'Product deleted permanently!');
  }

  public function show($id)
  {
    $product = Product::find($id);
    return view('admin.products.show', ['product' => $product]);
  }
  public function create()
  {
    $categories = Category::all();
    return view('admin.products.create', ['categories' => $categories]);
  }
  public function imageReplace($id)
  {
    request()->validate([
      'productImage' => ['required', 'max:1024', 'image'],
    ]);
    $product = Product::find($id);
    $product->clearMediaCollection('productImages');
    $product->addMediaFromRequest('productImage')->toMediaCollection('productImages');
    return \Redirect::route('admin-products-edit', [$id]);
    // code...
  }
  public function edit($id)
  {
    $product = Product::find($id);
    $categories = Category::all();
    return view('admin.products.edit', ['product' => $product, 'categories' => $categories]);
  }
  public function store(Request $request)
  {
    $characters = 'ABCDEFGHIJ';

    // generate a pin based on 2 * 7 digits + a random character
    $pin = mt_rand(10, 99999)
      . mt_rand(100, 99999)
      . $characters[rand(0, strlen($characters) - 1)];

    // shuffle the result
    $productSku = str_shuffle($pin);
    $categories = request()->categoryId;

    //validation
    request()->validate([
      'productName' => ['required', 'max:100', 'min:3'],
      'productDescription' => ['required', 'min:5'],
      'productImage' => ['required', 'max:2048', 'image'],
      'productPrice' => ['required', 'numeric'],
      'productQuantity' => ['required', 'integer'],
      'categories' => ['exists:categories,id']
    ]);

    var_dump(request()->all());
    $product = new Product();
    $product->name = request('productName');
    $product->description = request('productDescription');
    $product->price = request('productPrice');
    $product->quantity = request('productQuantity');
    $product->sku = $productSku;
    $product->creator_id = Auth::user()->id;

    $product
      ->addMediaFromRequest('productImage')
      ->toMediaCollection('productImages');
    $product->slug = str_slug(request('productName'), "-");
    $product->save();

    $category_ids = request()->categories;
    $product->categories()->sync($category_ids);

    return redirect('/admin/products')->with('success', 'Product created successfully!');;

    // code...
  }
  public function update($id, Request $request)
  {
    request()->validate([
      'productName' => ['required', 'max:100', 'min:3'],
      'productDescription' => ['required', 'min:5'],
      'productImage' => ['max:2048', 'image'],
      'productPrice' => ['required', 'numeric'],
      'productQuantity' => ['required', 'integer'],
      'categories' => ['exists:categories,id']
    ]);
    if ($request->isMethod('post')) {
      $data = $request->all();
      $product = Product::find($id);
      $product->name = request('productName');
      $product->description = request('productDescription');
      $product->quantity = request('productQuantity');
      //  $product->sku = request('productSku');
      $product->price = request('productPrice');

      if ($request->hasfile('productImage')) {

        $product->clearMediaCollection('productImages');

        $product->addMediaFromRequest('productImage')->toMediaCollection('productImages');
      }
    }
    $product->save();
    return redirect('/admin/products')->with('success', 'Product updated successfully!');;
  }
  public function destroy($id)
  {
    $user = Product::find($id)->delete();
    return redirect()->route('admin-products')->with('success', 'Product deleted successfully!');;
  }
}
