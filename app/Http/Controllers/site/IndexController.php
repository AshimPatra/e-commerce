<?php

namespace App\Http\Controllers\site;

use App\Category;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Auth;
use App\Cart;

use Session;

class IndexController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $category = Category::all();
        $categories = Category::whereNull('parent_id')->get();
        return view('site.index', ['category' => $category, 'parentCategories' => $categories, 'products' => $products]);
    }
    public function about()
    {
        return view('site.about');
    }
    public function shop()
    {
        $count = 6;

        $products = Product::all();

        $maxPrice = Product::max('price');
        $products = new Product;
        $q = [];
        if (request()->has('category')) {
            $products = new Category;
            $products = $products->find(request()->category)->products();
            $q['category'] = request()->category;
        }
        if (request()->has('priceLow')) {
            $priceHigh = explode(' ', request()->priceHigh)[1];
            $priceLow = explode(' ', request()->priceLow)[1];
            $q['priceLow'] = $priceLow;
            $q['priceHigh'] = $priceHigh;
            $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
        }
        $products = $products->paginate($count)->appends($q);
        // $categories = Category::whereNull('parent_id')->get();
        // return view('site.shop',['products'=>$products, 'parentCategories'=>$categories, 'count'=>$count, 'maxPrice'=>$maxPrice]);

        return view('site.shop', ['products' => $products,  'count' => $count, 'maxPrice' => $maxPrice]);
    }
    public function blog()
    {
        return view('site.blog');
    }
    public function contact()
    {
        return view('site.contact');
    }
    public function category($slug)
    {
        $products = Product::all();
        $count = 3;
        $maxPrice = Product::max('price');
        $q = [];
        $category = Category::where('slug', '=', $slug)->firstOrFail();
        $categoriesSideBar = Category::whereNull('parent_id')->get();
        $products = $category->products();
        if (request()->has('subcategory')) {
            $products = new Category;
            $products = $products->find(request()->subcategory)->products();
            $q['category'] = request()->subcategory;
        }
        if (request()->has('priceLow')) {
            $priceHigh = explode(' ', request()->priceHigh)[1];
            $priceLow = explode(' ', request()->priceLow)[1];
            $q['priceLow'] = $priceLow;
            $q['priceHigh'] = $priceHigh;
            $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
        }
        $products = $products->paginate($count)->appends($q);

        return view('site.category', ['category' => $category, 'parentCategories' => $categoriesSideBar, 'count' => $count, 'maxPrice' => $maxPrice, 'products' => $products]);
    }
    public function subcategory($parent, $slug)
    {
        $products = Product::all();
        $count = 3;
        $maxPrice = Product::max('price');
        $q = [];
        $category = Category::where('slug', '=', $slug)->firstOrFail();
        $categoriesSideBar = Category::whereNull('parent_id')->get();
        $products = $category->products();
        if (request()->has('subcategory')) {
            $products = new Category;
            $products = $products->find(request()->subcategory)->products();
            $q['category'] = request()->subcategory;
        }
        if (request()->has('priceLow')) {
            $priceHigh = explode(' ', request()->priceHigh)[1];
            $priceLow = explode(' ', request()->priceLow)[1];
            $q['priceLow'] = $priceLow;
            $q['priceHigh'] = $priceHigh;
            $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
        }
        $products = $products->paginate($count)->appends($q);

        return view('site.category', ['category' => $category, 'parentCategories' => $categoriesSideBar, 'count' => $count, 'maxPrice' => $maxPrice, 'products' => $products]);
    }
    public function getAddToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);

        return redirect()->route('site-shop');
        //return view('site.pages.cart');
    }
    public function getCart()
    {

        if (!Session::has('cart')) {
            return view('site.pages.cart', ['products' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('site.pages.cart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    public function checkout()
    {
        return view('site.pages.checkout');
    }



    public function productDetail($slug)
    {
        $products = Product::all();

        $product = Product::where('slug', '=', $slug)->firstOrFail();
        // $categories = Category::whereNull('parent_id')->get();
        return view('site.pages.productDetail', ['product' => $product, 'products' => $products]);
    }
    public function address()
    {
        return view('site.account.address');
    }
    public function dashboard()
    {
        return view('site.account.dashboard');
    }
    public function edit()
    {
        return view('site.account.edit');
    }
    public function changePassword()
    {
        return view('site.account.changePassword');
    }
    public function reduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        Session::put('cart', $cart);
        return redirect()->route('site-shopping-cart');
    }
    public function getRemoveItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        Session::put('cart', $cart);
        return redirect()->route('site-shopping-cart');
    }
}
