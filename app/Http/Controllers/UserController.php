<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Session;

class UserController extends Controller
{
  public function index(Request $request)
  {
    if ($request->ajax()) {
      $data = User::latest()->get();
      return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('action', function ($row) {
          $btn = '<a href="/admin/users/' . $row['id'] . '/edit" class="edit btn btn-primary btn-sm"> <i class="fa fa-edit"></i></a>';
          $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/users/' . $row['id'] . '/delete" class="delete btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
          return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    return view('admin.users.index');
  }
  public function restore($id)
  {

    $user = User::withTrashed()->find($id);
    if (empty($user)) {
      return view('admin.trash', ['#pills-user']);
    }
    $user->restore();
    return redirect()->route('admin-trash', ['#pills-user'])->with('success', 'User restore successfully!');
  }
  public function permanentDelete($id)
  {
    $user = User::withTrashed()->find($id);
    if (empty($user)) {
      return redirect()->route('admin-trash', ['#pills-user']);
    }
    $user->forceDelete();
    return redirect()->route('admin-trash', ['#pills-user'])->with('success', 'User deleted permanently!');
  }
  public function dashboard()
  {
    return view('admin.dashboard');
  }
  public function login()
  {
    return view('auth.login');
  }
  public function register()
  {
    return view('auth.register');
  }
  public function create()
  {
    return view('admin.users.create');
  }
  public function store(Request $request)
  {
    request()->validate([
      'name' => ['required', 'max:255', 'min:3'],
      'password' => ['required', 'min:5', 'max:255'],
      'email' => ['required', 'email', 'max:255', 'unique:users,email'],
      'type' => ['required'],
      'phone' => ['required', 'digits:10'],
    ]);
    $user = new User();
    $user->name = request()->name;
    $user->password = Hash::make(request()->password);
    $user->email = request()->email;
    $user->phone = request()->phone;
    $user->type = request()->type;
    $user->save();

    return redirect('/admin/users')->with('success', 'User created successfully!');
  }
  public function edit($id)
  {
    $user = User::find($id);

    return view('admin.users.edit', ['user' => $user]);
  }
  public function update($id)
  {
    request()->validate([
      'name' => ['required', 'max:255', 'min:3'],
      'email' => ['required', 'email', 'max:255',],
      'type' => ['required'],
      'phone' => ['required', 'digits:10'],
    ]);
    $user = User::find($id);
    $user->name = request()->name;
    $user->email = request()->email;
    $user->phone = request()->phone;
    $user->type = request()->type;
    $user->save();
    return redirect('/admin/users')->with('success', 'User updated successfully!');
  }
  public function destroy($id)
  {
    $msg = "Cannot delete active user";
    $user = User::find($id);
    if ($user->id != Auth::user()->id) {
      $user->delete();
      $msg = "User deleted succesfully";
    }
    return redirect()->route('admin-users')->with('success', $msg);
  }
  public function settings()
  {
    return view('admin.settings');
  }
}
