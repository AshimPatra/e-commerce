<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Spatie\MediaLibrary\Models\Media;

use Spatie\Image\Manipulations;

class Product extends Model implements HasMedia
{

    use HasMediaTrait;
    use SoftDeletes;
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'product_category', 'category_id', 'product_id');
    }
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(624)
            ->height(800)
            ->fit(Manipulations::FIT_STRETCH, 624, 800)
            ->sharpen(10);
    }
}
