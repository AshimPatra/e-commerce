<?php

namespace App;

use App\Models\Product;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;



use Spatie\MediaLibrary\Models\Media;

use Spatie\Image\Manipulations;

class Category extends Model implements HasMedia
{
    use HasMediaTrait;
    use SoftDeletes;
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_category', 'category_id',  'product_id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(1170)
            ->height(500)
            ->crop('crop-center', 1170, 500);
    }
}
