@extends('layouts.admin')
@section('title', 'Create Category')

@section('content-header')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Add Category</h1>
        </div>
    </div>
</div><!-- /.container-fluid -->


@endsection

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Enter Category Details</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="post" enctype="multipart/form-data" action="/admin/categories">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col form-group">
                            <label for="name">Name</label>
                            @error('categoryName')
                            <p class="text-danger">{{$errors->first('categoryName')}}</p>
                            @enderror
                            <input type="text" value="{{ old('categoryName') }}" name="categoryName" class="form-control" id="categoryName" placeholder="Enter Name">
                        </div>
                        <div class="col form-group">
                            <label>Picture upload</label><br>
                            @error('categoryImage')
                            <p class="text-danger">{{$errors->first('categoryImage')}}</p>
                            @enderror
                            <input type="file" name="categoryImage">
                            <input type="hidden">
                        </div>
                        <div class="col form-group">
                            <label for="categoryImage">Parent Category</label>
                            <select class="form-control select2 select2-hidden-accessible" name="parentCategory" id="parentCategory" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                <option value="">Root</option>
                                @foreach ($categories->whereNull('parent_id') as $category)
                                <option @if(old('parentCategory')==$category->id){{"selected"}} @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Product Description</label>
                        @error('categoryDescription')
                        <p class="text-danger">{{$errors->first('categoryDescription')}}</p>
                        @enderror
                        <textarea class="form-control" name="categoryDescription" rows="3" placeholder="Enter product description">{{ old('categoryDescription') }}</textarea>
                    </div>
                    <!-- /.card-body -->
                    <div class="">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>
@endsection