@extends('layouts.admin')
@section('title', 'Edit Category')

@section('content-header')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Edit Category</h1>
        </div>

    </div>
</div><!-- /.container-fluid -->
@endsection

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Enter Category Details</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="post" enctype="multipart/form-data" action="/admin/categories/{{$category->id}}">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col form-group">
                            <label for="cetegorySlug">Slug</label>
                            <input value="{{ $category->slug }}" name="categorySlug" type="text" class="form-control" readonly id="categorySlug">
                        </div>
                        <div class="col form-group">
                            <label for="name">Name</label>
                            @error('categoryName')
                            <p class="text-danger">{{$errors->first('categoryName')}}</p>
                            @enderror
                            <input type="text" name="categoryName" value="{{ $errors->any() ? old('categoryName'):$category->name}} " class=" form-control" id="categoryName" placeholder="Enter Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Category Description</label>
                        @error('categoryDescription')
                        <p class="text-danger">{{$errors->first('categoryDescription')}}</p>
                        @enderror
                        <textarea class="form-control" id="categoryDescription " name=" categoryDescription" rows="3" placeholder="Enter category description">{{ $errors->any() ? old('categoryDescription'):$category->description}}</textarea>

                    </div>
                    <div class="form-group">
                        <label>Picture upload</label><br>
                        <input id="categoryImage" type="file" name="categoryImage">
                        <br>
                        <img style="width:50px;margin-top:5px;" src="{{ $category->getFirstMediaUrl('categoryImages') }}">
                    </div>

                    <!-- /.card-body -->
                    <div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>
@endsection