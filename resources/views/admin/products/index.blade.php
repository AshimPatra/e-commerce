@extends('layouts.admin')
@section('title', 'Show Products')
@section('header-content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
@endsection
@section('content-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Products List</h1>
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
    </div>
  </div>
</div><!-- /.container-fluid -->
@endsection

@section('content')


<div class="card">

  <!-- /.card-header -->
  <div class="card-body">
    <div class="dataTables_wrapper dt-bootstrap4">
      <div class="row">
        <div class="col-sm-12 col-md-6"></div>
        <div class="col-sm-12 col-md-6"></div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table  data-table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
            <thead>
              <tr>
                <th>No</th>
                <th>Sku</th>
                <th>Slug</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>creator</th>
                <th>image</th>

                <th width="100px">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.card-body -->
</div>

@endsection
@section('script-content')
<script type="text/javascript">
  $(function() {
    var table = $('.data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: "{{ route('admin-products') }}",

      columns: [{
          data: 'DT_RowIndex',
          name: 'DT_RowIndex'
        },
        {
          data: 'sku',
          name: 'sku'
        },
        {
          data: 'slug',
          name: 'slug'
        },
        {
          data: 'name',
          name: 'name'
        },
        {
          data: 'quantity',
          name: 'quantity'
        },
        {
          data: 'price',
          name: 'price'
        },
        {
          data: 'creator',
          name: 'creator'
        },
        {
          data: 'image',
          name: 'image'
        },
        {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false
        },
      ]
    });
  });
</script>
@endsection