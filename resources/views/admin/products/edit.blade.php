@extends('layouts.admin')

@section('title', 'Edit Product')
@section('header-content')
<link rel="stylesheet" href="/admin-lte/plugins/select2/css/select2.min.css">
@endsection
@section('content-header')

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Edit Products</h1>
    </div>
  </div>
</div><!-- /.container-fluid -->
@endsection

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Enter Product Details</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form id="quickForm" enctype="multipart/form-data" method="POST" action="/admin/products/{{$product->id}}">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="col orm-group">
                  <label for="productName">Product Name</label>
                  @error('productName')
                  <p class="text-danger">{{$errors->first('productName')}}</p>
                  @enderror
                  <input type="text" name="productName" class="form-control" id="productName" placeholder="Enter product name" value="{{ $errors->any() ? old('productName'):$product->name}}">
                </div>
                <div class="col form-group">
                  <label for="productPrice">Price</label>
                  @error('productPrice')
                  <p class="text-danger">{{$errors->first('productPrice')}}</p>
                  @enderror
                  <input type="text" name="productPrice" class="form-control" id="productPrice" placeholder="Price" value="{{ $errors->any() ? old('productPrice'):$product->price}}">
                </div>
                <div class="col form-group">
                  <label for="productQuantity">Quantity</label>
                  @error('productQuantity')
                  <p class="text-danger">{{$errors->first('productQuantity')}}</p>
                  @enderror
                  <input value="{{ $errors->any() ? old('productQuantity'):$product->quantity}}" type="text" name="productQuantity" class="form-control" id="productQuantity" placeholder="Quantity">
                </div>
              </div>

              <div class="row">

                <div class="col form-group" data-select2-id="63">
                  <label>category</label>
                  <select class="select2 select2-hidden-accessible" name="categoryId " multiple="" data-placeholder="Select a State" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                    @foreach ($categories as $category)
                    <option>{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col form-group">
                  <label for="productSku">Sku</label>
                  <input value="{{ $product->sku }}" type="text" name="productSlug" class="form-control" readonly id="productSku">
                </div>
                <div class="col form-group">
                  <label for="productslug">Slug</label>
                  <input value="{{ $product->slug }}" name="productSlug" type="text" class="form-control" readonly id="productSku">
                </div>
              </div>
              <div class=" form-group">
                <label>Description</label>
                @error('productDescription')
                <p class="text-danger">{{$errors->first('productDescription')}}</p>
                @enderror
                <textarea class="form-control" name="productDescription" rows="3" placeholder="Enter product description">{{ $errors->any() ? old('productDescription'):$product->description}}</textarea>
              </div>


              <div class="form-group">
                <label>Picture upload</label><br>
                <input type="file" name="productImage">
                <br>
                <img style="width:50px;margin-top:5px;" src="{{ $product->getFirstMediaUrl('productImages') }}">
              </div>

              <!-- /.card-body -->
              <div>
                <button type="submit" class="btn btn-primary">Update Product</button>
              </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
@endsection
@section('script-content')
<script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
<script>
  $(function() {
    //Initialize Select2 Elements
    $('.select2').select2();
  });
</script>
@endsection