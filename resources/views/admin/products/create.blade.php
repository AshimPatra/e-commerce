@extends('layouts.admin')

@section('title', 'Add Product')
@section('header-content')
<link rel="stylesheet" href="/admin-lte/plugins/select2/css/select2.min.css">
@endsection
@section('content-header')

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Add Products</h1>
    </div>
  </div>
</div><!-- /.container-fluid -->
@endsection


@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Enter Product Details</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form id="quickForm" enctype="multipart/form-data" method="POST" action="/admin/products">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="productName">Product Name</label>
                @error('productName')
                <p class="text-danger">{{$errors->first('productName')}}</p>
                @enderror
                <input type="text" name="productName" class="form-control" id="productName" placeholder="Enter product name" value="{{ old('productName') }}">
              </div>



              <div class="row">
                <div class="col form-group">
                  <label for="productPrice">Product Price</label>
                  @error('productPrice')
                  <p class="text-danger">{{$errors->first('productPrice')}}</p>
                  @enderror
                  <input type="text" name="productPrice" class="form-control" id="productPrice" placeholder="Price" value="{{ old('productPrice') }}">
                </div>


                <div class="col form-group">
                  <label for="productQuantity">Product Quantity</label>
                  @error('productQuantity')
                  <p class="text-danger">{{$errors->first('productQuantity')}}</p>
                  @enderror
                  <input type="text" name="productQuantity" value="{{ old('productQuantity') }}" class="form-control" id="productQuantity" placeholder="Quantity">
                </div>


                <div class="col form-group" data-select2-id="63">
                  <label>Category</label>
                  @error('categories')
                  <p class="text-danger">{{$errors->first('categories')}}</p>
                  @enderror
                  <select class="select2" name="categories[]" id="categories" multiple="multiple" data-placeholder="Select categories" style="width: 100%;">
                    @foreach ($categories as $category)
                    <option @if(old('categories') !=null && in_array( $category->id, old('categories'))){{"selected"}} @endif value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label>Product Description</label>
                @error('productDescription')
                <p class="text-danger">{{$errors->first('productDescription')}}</p>
                @enderror
                <textarea class="form-control" name="productDescription" rows="3" placeholder="Enter product description">{{ old('productDescription') }}</textarea>
              </div>
              <div class="row">
                <div class="col form-group">
                  <label for="exampleInputFile">Product Image</label>
                  @error('productImage')
                  <p class="text-danger">{{$errors->first('productImage')}}</p>
                  @enderror
                  <div class="input-group">
                    <input name="productImage" type="file" id="exampleInputFile">

                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Add Product</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
@endsection
@section('script-content')
<script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
<script>
  $(function() {
    //Initialize Select2 Elements
    $('.select2').select2();
  });
</script>
@endsection