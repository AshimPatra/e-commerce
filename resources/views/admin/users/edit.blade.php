@extends('layouts.admin')
@section('title', 'Create User')

@section('content-header')

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Edit User</h1>

    </div>
  </div>
</div><!-- /.container-fluid -->


@endsection

@section('content')
<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- jquery validation -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Alter User Details</small></h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form id="quickForm" method="post" action="/admin/users/{{$user->id}}">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="name">Name</label>
            @error('name')
            <p class="text-danger">{{$errors->first('name')}}</p>
            @enderror
            <input type="text" value="{{ $errors->any() ? old('name'):$user->name}}" name="name" class="form-control" id="name" placeholder="Enter Name">
          </div>


          <div class="row">
            <div class="col form-group">
              <label for="email">Email address</label>
              @error('email')
              <p class="text-danger">{{$errors->first('email')}}</p>
              @enderror
              <input type="email" value="{{ $errors->any() ? old('email'):$user->email}}" name="email" class="form-control" id="email" placeholder="Enter email">
            </div>

            <div class="col form-group">
              <label for="phone">Phone Number</label>
              @error('phone')
              <p class="text-danger">{{$errors->first('phone')}}</p>
              @enderror
              <input type="text" name="phone" value="{{ $errors->any() ? old('phone'):$user->phone}}" class="form-control" id="phone" placeholder="Enter Name">
            </div>

            <div class="col form-group">
              <label>Type</label>
              @error('type')
              <p class="text-danger">{{$errors->first('type')}}</p>
              @enderror
              <select name="type" class="custom-select">
                <option {{ $errors->any() ? ((old('type') == "admin") ? "selected" : "") :(($user["type"] == "admin") ? "selected" : "")}} value='admin'>Admin</option>
                <option {{ $errors->any() ? ((old('type') == "customer") ? "selected" : ""):(($user["type"] == "admin") ? "selected" : "")}} value='customer'>Customer</option>
              </select>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.card -->
  </div>
  <!--/.col (left) -->
  <!-- right column -->
  <div class="col-md-6">

  </div>
  <!--/.col (right) -->
</div>
@endsection