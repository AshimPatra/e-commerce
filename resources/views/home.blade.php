@extends('layouts.site')

@section('title', 'Account')

@section('content')
<div class="container mt_30">
    <div class="row ">
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.account_sidebar')
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <div class="panel-group">
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <form id="quickForm" method="post">
                            <div id="dashboard" class="tabcontent">
                                <div class="row">
                                    <div class="col col-md-4">
                                        <div class="sdw-block">
                                            <div class="wrap bg-white">

                                                <!-- Caption -->
                                                <div class="caption">

                                                    <!-- Header -->
                                                    <span class="header text-uppercase" style="font-size:4rem">
                                                        150
                                                    </span>
                                                    <p>Orders Placed</p>
                                                    <!-- Text -->


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-md-4">
                                        <div class="sdw-block">
                                            <div class="wrap bg-white">

                                                <!-- Caption -->
                                                <div class="caption">

                                                    <!-- Header -->
                                                    <span class="header text-uppercase" style="font-size:4rem">
                                                        1500
                                                    </span>
                                                    <p>Items Bought</p>
                                                    <!-- Text -->


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-md-4">
                                        <div class="sdw-block">
                                            <div class="wrap bg-white">

                                                <!-- Caption -->
                                                <div class="caption">

                                                    <!-- Header -->
                                                    <span class="header text-uppercase" style="font-size:4rem">
                                                        30
                                                    </span>
                                                    <p>Orders Returned</p>
                                                    <!-- Text -->


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
<!-- Single Blog  -->
<!-- End Blog   -->
<!-- =====  CONTAINER END  ===== -->