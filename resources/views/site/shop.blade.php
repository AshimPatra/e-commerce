@extends('layouts.site')
@section('title', 'Shop')
@section('content')


<!-- =====  CONTAINER START  ===== -->
<div class="container">
    <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
            <div class="breadcrumb ptb_20">
                <h1>Products</h1>
                <ul>
                    <li><a href="{{ route('site-home')}}">Home</a></li>
                    <li class="active">Products</li>
                </ul>
            </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
        <div id="column-left" class="col-sm-4 col-lg-3 ">
            @include('layouts.site.sidebar')
            <div class="filter left-sidebar-widget mb_50">
                <div class="heading-part mtb_20 ">
                    <h2 class="main_title">Refinr Search</h2>
                </div>
                <div class="filter-block">
                    <p>
                        <label for="amount">Price range:</label>
                        <input type="text" id="amount" readonly>
                        <div id="slider-range" class="mtb_20 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                            <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 15%; width: 35%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 15%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 50%;"></span>
                        </div>
                    </p>
                    <div id="slider-range" class="mtb_20"></div>
                    <div class="list-group">
                        <div class="list-group-item mb_10">
                            <label>Size</label>
                            <div id="filter-group3">
                                <div class="checkbox">
                                    <label>
                                        <input value="" type="checkbox"> Big (3)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input value="" type="checkbox"> Medium (2)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input value="" type="checkbox"> Small (1)</label>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn">Refine Search</button>
                    </div>
                </div>
            </div>
            <div class="left_banner left-sidebar-widget mb_50"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>

        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <div class="category-page-wrapper mb_30">
                <div class="list-grid-wrapper pull-left">
                    <div class="btn-group btn-list-grid">
                        <button type="button" id="grid-view" class="btn btn-default grid-view active"></button>
                        <button type="button" id="list-view" class="btn btn-default list-view"></button>
                    </div>
                </div>
                <div class="page-wrapper pull-right">
                    <label class="control-label" for="input-limit">Show :</label>
                    <div class="limit">
                        <select id="input-limit" class="form-control">
                            <option value="8" selected="selected">08</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                </div>
                <div class="sort-wrapper pull-right">
                    <label class="control-label" for="input-sort">Sort By :</label>
                    <div class="sort-inner">
                        <select id="input-sort" class="form-control">
                            <option value="ASC" selected="selected">Default</option>
                            <option value="ASC">Name (A - Z)</option>
                            <option value="DESC">Name (Z - A)</option>
                            <option value="ASC">Price (Low &gt; High)</option>
                            <option value="DESC">Price (High &gt; Low)</option>
                            <option value="DESC">Rating (Highest)</option>
                            <option value="ASC">Rating (Lowest)</option>
                            <option value="ASC">Model (A - Z)</option>
                            <option value="DESC">Model (Z - A)</option>
                        </select>
                    </div>
                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                </div>
            </div>
            <div class="row">

                @foreach($products as $product)
                <div class="product-layout product-grid col-md-4 col-xs-6 ">
                    <div class="item">
                        <div class="product-thumb clearfix mb_30">
                            <div class="image product-imageblock"> <a href="{{route('site-productDetail',['slug'=>$product->slug])}}"> <img data-name="product_image" alt="iPod Classic" title="iPod Classic" src="{{$product->getFirstMediaUrl('productImages','thumb')}}" class="img-responsive" /> <img src="{{$product->getFirstMediaUrl('productImages','thumb')}}" alt="iPod Classic" title="iPod Classic" class="img-responsive" /> </a>
                                <div class="button-group text-center">

                                    <!-- <a class="quickview"><a href="{{route('site-productDetail',['slug'=>$product->slug])}}"><span>Quick View</span></a></div> -->
                                    <a href="{{route('site-productDetail',['slug'=>$product->slug])}}" type="button" class="addtocart-btn"><i class="fa fa-lg fa-eye"></i></a>
                                    <a href="{{ route('site-add-cart',['id'=> $product->id])}}" type="button" class="addtocart-btn"><i class="fa-lg fa fa-shopping-cart"></i></a>
                                    <!-- <div class="add-to-cart"><a href="{{route('site-productDetail',['slug'=>$product->slug])}}"></a></div> -->
                                </div>
                            </div>
                            <div class="caption product-detail text-center">
                                <h6 data-name="product_name" class="product-name mt_20"><a href="#" title="Casual Shirt With Ruffle Hem">{{$product->name}}</a></h6>

                                <span class="price"><span class="amount"><span class="currencySymbol">Rs </span>{{$product->price}}</span>
                                </span>
                                <p class="product-desc mt_20 mb_60"> {{$product->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="pagination-nav text-center mt_50">
                <ul>
                    <li><a href="{{route('site-shop', [ 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}"><i class="fa fa-angle-left"></i></a></li>
                    @for ($i = 1; $i <= ceil($products->total()/$count); $i++)
                        <li @if($i==request()->page || (request()->page == null && $i==1)) class='active' @endif><a href="{{route('site-shop',['page'=>$i, 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category ])}}">{{$i}}</a></li>
                        @endfor
                        <li><a href="{{route('site-shop',['page'=>ceil($products->total()/$count),  'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}"><i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>

        </div>
    </div>

</div>
<!-- =====  CONTAINER END  ===== -->
@endsection