@extends('layouts.site')
@section('title', 'productDetail')
@section('content')

<!-- =====  CONTAINER START  ===== -->
<!-- =====  CONTAINER START  ===== -->
<div class="container">
    <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
            <div class="breadcrumb ptb_20">
                <h1>{{$product->name}}</h1>
                <ul>
                    <li><a href="{{ route('site-home')}}">Home</a></li>
                    <li><a href="{{ route('site-shop')}}">shop</a></li>
                    <li class="active">{{$product->name}}</li>
                </ul>
            </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.sidebar')
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
            <div class="left-special left-sidebar-widget mb_50">
            </div>
        </div>

        <div class="col-sm-8 col-lg-9 mtb_20">
            <div class="row mt_10 ">
                <div class="col-md-6">

                    <div><a class="thumbnails"> <img data-name="product_image" src="{{$product->getFirstMediaUrl('productImages','thumb')}}" alt="" /></a></div>

                </div>
                <div class="col-md-6 prodetail caption product-thumb">
                    <h4 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">{{$product->name}}</a></h4>

                    <span class="price mb_20"><span class="amount"><span class="currencySymbol">Rs </span>{{$product->price}}</span>
                    </span>
                    <hr>
                    <ul class="list-unstyled product_info mtb_20">

                        <label>Product Code:</label>
                        <span>{{$product->sku}}</span></li>
                        <li>
                            <label>Availability:</label>
                            <span> In Stock</span></li>
                    </ul>
                    <hr>
                    <p class="product-desc mtb_30"> {{$product->description}}</p>
                    <div id="product">
                        <div class="form-group">
                            <div class="row">
                                <div class="Sort-by col-md-6">
                                    <label>Sort by</label>
                                    <select name="product_size" id="select-by-size" class="selectpicker form-control">
                                        <option>Small</option>
                                        <option>Medium</option>
                                        <option>Large</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="qty form-group2">
                            <label>Qty</label><br>
                            <input name="product_quantity" min="1" value="1" type="number">
                        </div>
                        <div class="mt_30">
                            <a href="{{ route('site-add-cart',['id'=> $product->id])}}" class="btn " role="button">Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="heading-part text-center mb_10">
                        <h2 class="main_title mt_50">Related Products</h2>
                    </div>
                    <div class="related_pro box">
                        <div class="product-layout  product-grid related-pro  owl-carousel mb_50 ">
                            @foreach($products as $productss)
                            <div class="item">
                                <div class="product-thumb">
                                    <div class="image product-imageblock"> <a href="{{route('site-productDetail',['slug'=>$productss->slug])}}"> <img data-name="product_image" src="{{$productss->getFirstMediaUrl('productImages','thumb')}}" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="{{$productss->getFirstMediaUrl('productImages','thumb')}}" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a>
                                        <div class="button-group text-center">
                                            <div class="quickview"><a href="{{route('site-productDetail',['slug'=>$productss->slug])}}"><span>Quick View</span></a></div>
                                            <div class="add-to-cart"><a href="#"><span>Add to cart</span></a></div>
                                        </div>
                                    </div>
                                    <div class="caption product-detail text-center">
                                        <h6 data-name="product_name" class="product-name mt_20"><a href="#" title="Casual Shirt With Ruffle Hem">{{$productss->name}}</a></h6>

                                        <span class="price"><span class="amount"><span class="currencySymbol">Rs</span>{{$productss->price}}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="brand_carouse" class="ptb_30 text-center">
        <div class="type-01">
            <div class="heading-part mb_10 ">
                <h2 class="main_title">Brand Logo</h2>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="brand owl-carousel ptb_20">
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand1.png" alt="Disney" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand2.png" alt="Dell" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand3.png" alt="Harley" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand4.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand6.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand7.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand8.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand9.png" alt="Canon" class="img-responsive" /></a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =====  CONTAINER END  ===== -->
<!-- =====  CONTAINER END  ===== -->
@endsection