@extends('layouts.site')
@section('title', 'Cart')
@section('content')

<div class="container">
    <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
            <div class="breadcrumb ptb_20">
                <h1>Shopping Cart</h1>
                <ul>
                    <li><a href="{{ route('site-home')}}">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ul>
            </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.sidebar')
            <div class="left_banner left-sidebar-widget mb_50 mt_30"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
            <div class="left-special left-sidebar-widget mb_50">

            </div>
        </div>
        @if(Session::has('cart'))
        <div class="col-sm-8 col-lg-9 mtb_20">
            @foreach($products as $product)


            <form enctype="multipart/form-data" method="post" action="#">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td class="text-center">Image</td>
                                <td class="text-left">Product Name</td>

                                <td class="text-left">Quantity</td>
                                <td class="text-right">Unit Price</td>
                                <td class="text-right">Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td class="text-center"><a href="#"><img src="product_image" alt="iPod Classic" title="iPod Classic" src="" alt="iPod Classic" title="iPod Classic"></a></td>
                                <td class="text-left"><a href="">{{$product['item']['name']}}</a></td>

                                <td class="text-left">
                                    <div style="max-width: 200px;" class="input-group btn-block">
                                        <input readonly type="text" class="form-control quantity" size="1" value=" {{$product['qty']}}" name="quantity">
                                        <span class="input-group-btn">
                                            <a href=" {{route('site-reduce-cart',['id'=>$product['item']['id']])}}" class="btn" title="" data-toggle="tooltip" type="submit" data-original-title="Reduce"><i class="fa fa-minus-circle"></i></i></a>
                                            <a href=" {{route('site-remove-cart',['id'=>$product['item']['id']])}}" class="btn btn-danger" title="" data-toggle="tooltip" type="button" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                                        </span>
                                    </div>
                                </td>
                                <td class="text-right">Rs {{$product['item']['price']}}</td>
                                <td class="text-right">Rs {{$product['price']}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </form>
            @endforeach
            <div class="row">
                <div class="col-sm-6 col-sm-offset ">
                    <table class="table table-bordered">
                        <tbody>

                            <tr>
                                <td class="text-right"><strong>Total:</strong></td>
                                <td class="text-right">Rs {{$totalPrice}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <form action="{{route('site-home')}}">
                <input class=" btn pull-left mt_30" type="submit" value="Continue Shopping" />
            </form>
            <form action="{{route('site-checkout')}}">
                <input class="btn pull-right mt_30" type="submit" value="Checkout" />
            </form>
        </div>
        @endif
    </div>
    <div id="brand_carouse" class="ptb_30 text-center">
        <div class="type-01">
            <div class="heading-part mb_20 ">
                <h2 class="main_title">Brand Logo</h2>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="brand owl-carousel ptb_20">
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand1.png" alt="Disney" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand2.png" alt="Dell" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand3.png" alt="Harley" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand4.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand6.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand7.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand8.png" alt="Canon" class="img-responsive" /></a> </div>
                        <div class="item text-center"> <a href="#"><img src="/assets/images/brand/brand9.png" alt="Canon" class="img-responsive" /></a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =====  CONTAINER END  ===== -->
@endsection