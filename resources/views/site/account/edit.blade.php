@extends('layouts.site')

@section('title', 'Contact')

@section('content')
<!-- =====  CONTAINER START  ===== -->
<div class="container mt_30">
    <div class="row ">
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.account_sidebar')
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <div class="panel-group">
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <form id="quickForm" method="post">
                            @csrf
                            <div class=" card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    @error('name')
                                    <p class="text-danger">{{$errors->first('name')}}</p>
                                    @enderror
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name">
                                </div>


                                <div class=" form-group">
                                    <label for="email">Email address</label>
                                    @error('email')
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                    @enderror
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                                </div>

                                <div class=" form-group">
                                    <label for="phone">Phone Number</label>
                                    @error('phone')
                                    <p class="text-danger">{{$errors->first('phone')}}</p>
                                    @enderror
                                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter Name">
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<!-- Single Blog  -->
<!-- End Blog   -->
<!-- =====  CONTAINER END  ===== -->
@endsection