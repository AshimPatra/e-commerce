@extends('layouts.site')

@section('title', 'Account')

@section('content')
<!-- =====  CONTAINER START  ===== -->
<div class="container mt_30">
    <div class="row ">
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.account_sidebar')
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <div class="panel-group">
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <form id="quickForm">
                            <br>
                            <div id="shipping-new">
                                <div class=" form-group required">
                                    <label for="old-password" class="control-label">Old Password</label>
                                    <div class="">
                                        <input type="text" class=" form-control" id="old-password" placeholder="Old pasword" value="" name="firstname">
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label for="new-password" class=" control-label">New Password</label>
                                    <div class="">
                                        <input type="text" class="form-control" id="new-Password" placeholder="New Password" value="" name="new-password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirm-password" class="control-label">Confirm Password</label>
                                    <div class="">
                                        <input type="text" class="form-control" id="confirm-password" placeholder="Confirm-password" value="" name="confirm-password">
                                    </div>
                                </div>
                                <div class="buttons clearfix">
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>
<!-- Single Blog  -->
<!-- End Blog   -->
<!-- =====  CONTAINER END  ===== -->
@endsection