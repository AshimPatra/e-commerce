@extends('layouts.site')
@section('title', 'Categories')
@section('content')
<!-- =====  CONTAINER START  ===== -->
<div class="container mt_30">
    <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
            <div class="breadcrumb ptb_20">
                <h1>Categories</h1>
                <ul>
                    <li><a href="{{ route('site-home')}}">Home</a></li>
                    <li><a href="{{ route('site-home')}}">Categories</a></li>

                    @if($category->parent==null)
                    <li><a href="{{'/category/'.$category->slug}}">{{$category->name}}</a></li>
                    @else
                    <li><a href="{{'/category/'.$category->parent->slug}}">{{$category->parent->name}}</a></li>
                    <li><a href="{{'/category/'.$category->parent->slug. '/'.$category->slug}}">{{$category->name}}</a></li>
                    @endif
                </ul>
            </div>
        </div>

        <!-- =====  BREADCRUMB END===== -->
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            <div id="category-menu" class="navbar collapse in mb_40" aria-expanded="true" style="" role="button">
                <div class="nav-responsive">
                    @include('layouts.site.sidebar')
                    </ul>
                </div>
            </div>
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <!-- about  -->
            <div class="row">
                <div class="col-md-12">
                    <figure> <img src="{{$category->getFirstMediaUrl('categoryImages','thumb')}}" alt="#"> </figure>
                </div>

                <div class="col-md-12">
                    <div class="about-text">
                        <div class="about-heading-wrap">
                            <h2 class="about-heading mb_20 mt_40 ptb_10">{{$category->name}}</h2>
                        </div>
                        <p>{{$category->description}}</p>
                    </div>
                </div>
            </div>
            <!-- =====  product ===== -->

            <!-- =====  end  ===== -->
            <!--Team Carousel -->
            <div class="heading-part mb_10">
                <h2 class="main_title mt_50">Sub Categories</h2>
            </div>
            <div class="team_grid box">
                <div class="team3col  owl-carousel">
                    @foreach($category->children as $subcategory)
                    <div class="item team-detail">
                        <div class="team-item-img"> <a href=" {{route('site-child-category', ['parent'=>$category->slug, 'slug'=>$subcategory->slug])}}"> <img src="{{$subcategory->getFirstMediaUrl('categoryImages','thumb')}}" alt="" /> </a></div>

                        <h4 class="team-title  mtb_10">{{$subcategory->name}}</h4>
                        <p>{{$subcategory->description}}</p>
                    </div>
                    @endforeach
                </div>
                <!--End Team Carousel -->
            </div>

            <!--product-->

        </div>

    </div>


</div>
<!-- Single Blog  -->
<!-- End Blog   -->
<!-- =====  CONTAINER END  ===== -->
@endsection