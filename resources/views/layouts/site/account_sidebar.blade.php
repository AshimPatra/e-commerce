<div id="category-menu" class="navbar collapse in mb_40" aria-expanded="true" style="" role="button">
    <div class="nav-responsive">
        <div class="heading-part">
            <h2 class="main_title">View User Profile</h2>
        </div>
        <ul class="nav  main-navigation collapse in">
            <li><a href="{{route('site-account-dashboard')}}" class="active">Dashboard</a></li>
            <li><a href="#">My Order</a></li>

            <li><a onclick="openTab(event, 'dashboard')" href="{{route('site-account-edit')}}">Edit Acount Details</a></li>
            <li><a onclick="openTab(event, 'address')" href="{{route('site-account-address')}}">Change Address</a></li>
            <li><a href="{{route('site-account-changePassword')}}">Change Password</a></li>
            <li>
                <a style="border-radius: 3px;" href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

        </ul>
    </div>
</div>