<!-- =====  HEADER START  ===== -->
<header id="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="header-top-left">
                        <div class="contact"><span class="hidden-xs hidden-sm hidden-md">Days a week from 9:00 am to 7:00 pm</span></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <ul class="header-top-right text-right">
                        <li class="account"><a href="{{ route('login')}} ">My Account</a></li>
                        <li class="language dropdown"> <span class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Language <span class="caret"></span> </span>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">English</a></li>
                                <li><a href="#">French</a></li>
                                <li><a href="#">German</a></li>
                            </ul>
                        </li>
                        <li class="currency dropdown"> <span class="dropdown-toggle" id="dropdownMenu12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Currency <span class="caret"></span> </span>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu12">
                                <li><a href="#">€ Euro</a></li>
                                <li><a href="#">£ Pound Sterling</a></li>
                                <li><a href="#">$ US Dollar</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="main-search mt_40">
                        <input id="search-input" name="search" value="" placeholder="Search" class="form-control input-lg" autocomplete="off" type="text">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                        </span> </div>
                </div>
                <div class="navbar-header col-xs-6 col-sm-4"> <a class="navbar-brand" href="{{ route('site-home')}}"> <img alt="themini" src="/assets/images/logo.png"> </a> </div>
                <div class="col-xs-6 col-sm-4 shopcart">
                    <div id="cart" class="btn-group btn-block mtb_40">
                        <button type="button" class="btn" data-target="#cart-dropdown" data-toggle="collapse" aria-expanded="true"><span id="shippingcart">Shopping cart</span><span id="cart-total">items ({{Session::has('cart')? Session::get('cart')->totalQty : ''}})</span> </button>
                    </div>
                    <div id="cart-dropdown" class="cart-menu collapse">
                        <ul>
                            <li>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td class="text-center"><a href="#"><img src="/assets/images/product/70x84.jpg" alt="iPod Classic" title="iPod Classic"></a></td>
                                            <td class="text-left product-name"><a href="#">MacBook Pro</a> <span class="text-left price">$20.00</span>
                                                <input class="cart-qty" name="product_quantity" min="1" value="1" type="number">
                                            </td>
                                            <td class="text-center"><a class="close-cart"><i class="fa fa-times-circle"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center"><a href="#"><img src="/assets/images/product/70x84.jpg" alt="iPod Classic" title="iPod Classic"></a></td>
                                            <td class="text-left product-name"><a href="#">MacBook Pro</a> <span class="text-left price">$20.00</span>
                                                <input class="cart-qty" name="product_quantity" min="1" value="1" type="number">
                                            </td>
                                            <td class="text-center"><a class="close-cart"><i class="fa fa-times-circle"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </li>
                            <li>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="text-right"><strong>Sub-Total</strong></td>
                                            <td class="text-right">$2,100.00</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right"><strong>Eco Tax (-2.00)</strong></td>
                                            <td class="text-right">$2.00</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right"><strong>VAT (20%)</strong></td>
                                            <td class="text-right">$20.00</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right"><strong>Total</strong></td>
                                            <td class="text-right">$2,122.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </li>
                            <li>
                                <form action="{{ route('site-shopping-cart')}}">
                                    <input class="btn pull-left mt_10" value="View cart" type="submit">
                                </form>
                                <form action="{{ route('site-checkout')}}">
                                    <input class="btn pull-right mt_10" value="Checkout" type="submit">
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <nav class="navbar">
                <p>menu</p>
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"> <span class="i-bar"><i class="fa fa-bars"></i></span></button>
                <div class="collapse navbar-collapse js-navbar-collapse">
                    <ul id="menu" class="nav navbar-nav">
                        <li> <a href="{{ route('site-home')}}">Home</a></li>

                        <li class="dropdown mega-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">categories</a>
                            <ul class="dropdown-menu mega-dropdown-menu row">
                                @foreach(DB::table('categories')->whereNull('parent_id')->get() as $category)
                                <li class="col-md-3">
                                    <ul>
                                        <li class="dropdown-header">{{$category->name}}</li>
                                        @foreach(DB::table('categories')->where('parent_id',$category->id)->get() as $childcategory)
                                        <li><a href=" {{route('site-child-category', ['parent'=>$category->slug, 'slug'=>$childcategory->slug])}}">{{$childcategory->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        <li> <a href="{{ route('site-shop')}}">Shop</a></li>
                        <!-- <li> <a href="{{ route('site-blog')}}">Blog</a></li> -->
                        <!-- <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages </a> -->
                        <ul class="dropdown-menu">

                            <li> <a href="{{ route('site-checkout')}}">Checkout</a></li>
                            <li> <a href="">Product Detail Page</a></li>
                        </ul>
                        </li>
                        <!-- <li> <a href="{{ route('site-about')}}">About us</a></li> -->
                        <li> <a href="{{ route('site-contact')}}">Contact us</a></li>
                    </ul>
                </div>
                <!-- /.nav-collapse -->
            </nav>
        </div>
    </div>
</header>
<!-- =====  HEADER END  ===== -->