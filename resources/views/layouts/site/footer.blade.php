<div class="footer pt_60">
    <div class="container">
        <div class="newsletters mt_30 mb_50">
            <div class="row">
                <div class="col-sm-6">
                    <div class="news-head pull-left">
                        <h2>Follow Our Updates !</h2>
                        <div class="new-desc">Be the First to know about our Fresh Arrivals and much more!</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="news-form pull-right">
                        <form onsubmit="return validatemail();" method="post">
                            <div class="form-group required">
                                <input name="email" id="email" placeholder="Enter Your Email" class="form-control input-lg" required="" type="email">
                                <button type="submit" class="btn btn-default btn-lg">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 footer-block">
                <h6 class="footer-title ptb_20">Information</h6>
                <ul>
                    <li><a href="{{ route('site-about')}}">About Us</a></li>
                    <li><a href="#">Delivery Information</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="{{ route('site-contact')}}">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-block">
                <h6 class="footer-title ptb_20">Services</h6>
                <ul>
                    <li><a href="#">Returns</a></li>
                    <li><a href="#">Site Map</a></li>
                    <li><a href="#">Wish List</a></li>
                    <li><a href="{{ route('login')}} ">My Account</a></li>
                    <li><a href="#">Order History</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-block">
                <h6 class="footer-title ptb_20">Extras</h6>
                <ul>
                    <li><a href="#">Brands</a></li>
                    <li><a href="#">Gift Certificates</a></li>
                    <li><a href="#">Affiliates</a></li>
                    <li><a href="#">Specials</a></li>
                    <li><a href="#">Newsletter</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-block">
                <h6 class="footer-title ptb_20">Contacts</h6>
                <ul>
                    <li>Mindfire Solution</li>
                    <li>Dlf near patia,bhubnaswar Odisha</li>
                    <li>(+91) 78708 55320</li>
                    <li>ashimpatra661@gmail.com</li>
                    <li><a href="https://www.mindfiresolutions.com/">www.mindfire.com</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom mt_60 ptb_20">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="social_icon">
                        <ul>
                            <li><a href="https://www.facebook.com/MindfireSolutions"><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-google"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/mindfire-solutions"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="copyright-part text-center">We respect your privacy.copyright (c) Mindfire Solutions 2007-2021.</div>
                </div>
                <div class="col-sm-4">
                    <div class="payment-icon text-right">
                        <ul>
                            <li><i class="fa fa-cc-paypal "></i></li>
                            <li><i class="fa fa-cc-visa"></i></li>
                            <li><i class="fa fa-cc-discover"></i></li>
                            <li><i class="fa fa-cc-mastercard"></i></li>
                            <li><i class="fa fa-cc-amex"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>