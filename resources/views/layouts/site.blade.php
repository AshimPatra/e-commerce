<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <!-- =====  BASIC PAGE NEEDS  ===== -->
    <meta charset="utf-8">
    <title>{{config('app.name')}} | @yield('title')</title>
    <!-- =====  SEO MATE  ===== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="distribution" content="global">
    <meta name="revisit-after" content="2 Days">
    <meta name="robots" content="ALL">
    <meta name="rating" content="8 YEARS">
    <meta name="Language" content="en-us">
    <meta name="GOOGLEBOT" content="NOARCHIVE">
    <!-- =====  MOBILE SPECIFICATION  ===== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="viewport" content="width=device-width">
    <!-- =====  CSS  ===== -->
    <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/owl.carousel.css">
    <link rel="shortcut icon" href="/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/apple-touch-icon-114x114.png">
</head>

<body>
    <!-- =====  LODER  ===== -->
    @include('layouts.site.loder')
    <!-- =====  HEADER START  ===== -->
    @include('layouts.site.header')
    <!-- =====  HEADER END  ===== -->
    <!-- =====  CONTAINER START  ===== -->
    @yield('content')
    <!-- =====  CONTAINER END  ===== -->
    <!-- =====  FOOTER START  ===== -->
    @include('layouts.site.footer')
    <!-- =====  FOOTER END  ===== -->
    </div>

    <a id="scrollup"></a>
    <script src="/assets/js/jQuery_v3.1.1.min.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.magnific-popup.js"></script>
    <script src="/assets/js/custom.js"></script>
    <script type="text/javascript">
        $('input[name=\'payment_address\']').on('change', function() {
            if (this.value == 'new') {
                $('#payment-existing').hide();
                $('#payment-new').show();
            } else {
                $('#payment-existing').show();
                $('#payment-new').hide();
            }
        });
        $('input[name=\'shipping_address\']').on('change', function() {
            if (this.value == 'new') {
                $('#shipping-existing').hide();
                $('#shipping-new').show();
            } else {
                $('#shipping-existing').show();
                $('#shipping-new').hide();
            }
        });
    </script>

</body>