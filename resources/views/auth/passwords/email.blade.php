@extends('layouts.site')
@section('title', 'Account')
@section('content')

<!-- =====  CONTAINER START  ===== -->

<div class="container mt_30">
    <div class="row ">
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.sidebar')
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <!-- contact  -->
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel-login panel">
                        <div class="panel-heading">
                            <div class="row mb_20">
                                <div class="text-center">
                                    <h4>Password assistance</h4>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="POST" action="{{ route('password.email') }}">
                                        @csrf
                                        @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                        @endif
                                        @error('email')
                                        <span class="invalid-feedback " style="color:red " role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="form-group mb-3">
                                            <input placeholder="email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="restart-submit" id="restart-submit" tabindex="4" class="form-control btn btn-register" value="Send Reset Link">
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Single Blog  -->
<!-- End Blog   -->
<!-- =====  CONTAINER END  ===== -->
@endsection