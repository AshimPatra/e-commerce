@extends('layouts.site')
@section('title', 'Account')
@section('content')

<!-- =====  CONTAINER START  ===== -->

<div class="container mt_30">
    <div class="row ">
        <div id="column-left" class="col-sm-4 col-lg-3 hidden-xs">
            @include('layouts.site.sidebar')
            <div class="left_banner left-sidebar-widget mt_30 mb_40"> <a href="#"><img src="/assets/images/left1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
        </div>
        <div class="col-sm-8 col-lg-9 mtb_20">
            <!-- contact  -->
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel-login panel">
                        <div class="panel-heading">
                            <div class="row mb_20">
                                <div class="col-xs-6">
                                    <a href="#login" class="active" id="login-form-link">Login</a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#register" id="register-form-link">Register</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="login-form" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        @error('email')
                                        <span class="invalid-feedback" role="alert" style="color:red ">
                                            <strong>{{ $message }}</strong>
                                        </span>

                                        @enderror

                                        <div class="form-group mb-3">
                                            <input placeholder="email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        </div>

                                        <div class="form-group mb-3">
                                            <input placeholder="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        </div>
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href=" {{ route('password.request') }}" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <form method="POST" id="register-form" action="{{ route('register') }}">
                                        @csrf

                                        @error('name')
                                        <span class="invalid-feedback" role="alert" style="color:red ">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror


                                        <div class="form-group">
                                            <input type="text" name="name" id="name" tabindex="1" class="form-control" placeholder="Username" value="{{ old('name') }}">
                                        </div>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert" style="color:red ">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="{{ old('email') }}">
                                        </div>
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert" style="color:red ">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <input type="text" name="phone" id="phone" tabindex="1" class="form-control" placeholder="Mobile No" value="{{ old('phone') }}">
                                        </div>


                                        <div class="form-group">
                                            <input id="password" placeholder="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                                        </div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert" style="color:red ">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group ">
                                            <input id="password-confirm" placeholder="Confirm password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function($) {

        $('#btnCadastrar').click(function() {
            console.log('Clicou no Cadastrar');
            var url = 'index.php?option=com_acymailing&ctrl=sub&task=optin&hiddenlists=2';

            jQuery.ajax({
                type: "POST",
                url: url,
                data: $('form').serialize(),
                success: function() {
                    alert('Recebemos seus dados');
                }
            });

        });

    });
</script>
<!-- Single Blog  -->
<!-- End Blog   -->
<!-- =====  CONTAINER END  ===== -->
@endsection