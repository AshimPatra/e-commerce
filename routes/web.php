<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//use Symfony\Component\Routing\Route;

//use Symfony\Component\Routing\Route;

Route::get('users', ['uses' => 'UserController@index', 'as' => 'users.index']);


Route::get('admin/products', 'ProductController@index')->name('admin-products')->middleware('admin');
Route::get('admin/products/show', 'ProductController@showProducts')->name('admin-products-table')->middleware('admin');
Route::post('admin/products', 'ProductController@store')->name('admin-products-store')->middleware('admin');
Route::get('admin/products/create', 'ProductController@create')->name('admin-products-create')->middleware('admin');
Route::get('admin/products/{product}/edit', 'ProductController@edit')->name('admin-products-edit')->middleware('admin');
Route::post('admin/products/{product}', 'ProductController@update')->name('admin-products-update')->middleware('admin');
Route::get('admin/products/{id}/delete', 'ProductController@destroy')->name('admin-products-destroy')->middleware('admin');

Route::get('admin/users', 'UserController@index')->name('admin-users')->middleware('admin');
Route::get('admin/users/create', 'UserController@create')->name('admin-users-create')->middleware('admin');
Route::post('admin/users', 'UserController@store')->name('admin-users-store')->middleware('admin');
Route::get('admin/users/{customer}/edit', 'UserController@edit')->name('admin-users-edit')->middleware('admin');
Route::post('admin/users/{customer}', 'UserController@update')->name('admin-users-update')->middleware('admin');
Route::get('admin/users/{id}/delete', 'UserController@destroy')->name('admin-users-destroy')->middleware('admin');

Route::get('admin/categories', 'CategoryController@index')->name('admin-categories')->middleware('admin');
Route::get('admin/categories/show', 'CategoryController@showcategory')->name('admin-categories-table')->middleware('admin');
Route::post('admin/categories', 'CategoryController@store')->name('admin-categories-store')->middleware('admin');
Route::get('admin/categories/create', 'CategoryController@create')->name('admin-categories-create')->middleware('admin');
Route::get('admin/categories/{category}/edit', 'CategoryController@edit')->name('admin-categories-edit')->middleware('admin');
Route::post('admin/categories/{category}', 'CategoryController@update')->name('admin-categories-update')->middleware('admin');
Route::get('admin/categories/{id}/delete', 'CategoryController@destroy')->name('admin-categories-destroy')->middleware('admin');


Auth::routes();
Route::get('admin/dashboard', 'UserController@dashboard')->name('admin-dashboard')->middleware('admin');
Route::get('/login', 'UserController@login')->name('login')->middleware('guest');
//Route::get('/register', 'UserController@register')->name('register')->middleware('guest');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('admin/settings', 'UserController@settings')->name('admin-settings')->middleware('admin');
Route::get('admin/trash', 'TrashController@index')->name('admin-trash')->middleware('admin');
Route::get('admin/trash/users', 'TrashController@userTrash')->name('admin-trash-users')->middleware('admin');
Route::get('admin/users/trash', 'TrashController@userTrash')->name('admin-users-trash')->middleware('admin');


Route::get('admin/users/{id}/restore', 'UserController@restore')->name('admin-users-restore')->middleware('admin');
Route::get('admin/users/{id}/permanentDelete', 'UserController@permanentDelete')->name('admin-users-permanentDelete')->middleware('admin');


Route::get('admin/products/trash', 'TrashController@productTrash')->name('admin-products-trash')->middleware('admin');


Route::get('admin/products/{id}/restore', 'ProductController@restore')->name('admin-products-restore')->middleware('admin');
Route::get('admin/products/{id}/permanentDelete', 'ProductController@permanentDelete')->name('admin-products-permanentDelete')->middleware('admin');

Route::get('admin/categories/trash', 'TrashController@CategoryTrash')->name('admin-categories-trash')->middleware('admin');


Route::get('admin/categories/{id}/restore', 'CategoryController@restore')->name('admin-categories-restore')->middleware('admin');
Route::get('admin/categories/{id}/permanentDelete', 'CategoryController@permanentDelete')->name('admin-categories-permanentDelete')->middleware('admin');
//site
Route::namespace('site')->group(function () {
    Route::get('/', 'IndexController@index');
    Route::get('site/home', 'IndexController@index')->name('site-home');
    Route::get('site/about', 'IndexController@about')->name('site-about');
    Route::get('site/shop', 'IndexController@shop')->name('site-shop');
    Route::get('site/blog', 'IndexController@blog')->name('site-blog');
    Route::get('site/contact', 'IndexController@contact')->name('site-contact');


    Route::get('site/add-to-cart/{id}', 'IndexController@getAddToCart')->name('site-add-cart');
    Route::get('site/shopping-cart', 'IndexController@getCart')->name('site-shopping-cart');
    Route::get('/reduce/{id}', 'IndexController@reduceByOne')->name('site-reduce-cart');
    Route::get('/remove/{id}', 'IndexController@getRemoveItem')->name('site-remove-cart');

    // Route::get('site/cart', 'IndexController@cart')->name('site-cart');
    //   Route::get('site/categories', 'IndexController@categories')->name('site-categories');
    Route::get('site/checkout', 'IndexController@checkout')->name('site-checkout')->middleware('auth');;
    // Route::get('site/cart', 'IndexController@cart')->name('cart');
    Route::get('/category/{parent}/{slug}', 'IndexController@subcategory')->name('site-child-category');
    Route::get('/category/{slug}', 'IndexController@category')->name('site-parent-category');
    Route::get('/product/{slug}', 'IndexController@productDetail')->name('site-productDetail');
    Route::get('site/account/dashboard', 'IndexController@dashboard')->name('site-account-dashboard');
    //My account
    Route::get('site/account/address', 'IndexController@address')->name('site-account-address');
    Route::get('site/account/changePassword', 'IndexController@changePassword')->name('site-account-changePassword');
    Route::get('site/account/edit', 'IndexController@edit')->name('site-account-edit');
    Route::get('site/account/dashboard', 'IndexController@dashboard')->name('site-account-dashboard');
});
